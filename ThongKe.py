import math
from cmath import nan
from turtle import onclick, width
import webbrowser
import streamlit as st
import pandas as pd
import numpy as np
import geopandas as gpd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import io
import xlsxwriter
from pathlib import Path
import pydeck as pdk
from leafmap.common import hex_to_rgb



## set up Layout
st.set_page_config(
     page_title="DATA ANALYTIC AND VISUALIZATION",
     page_icon="img/Icon.jpg",
     layout="wide",
     initial_sidebar_state="expanded",)


## BỐ TRÍ SIDEBAR
TAB1,TAB2,TAB3= st.sidebar.tabs(['TRANG CHỦ','BẢN ĐỒ','BẢNG - BIỂU'])

TAB2_1,TAB2_2 = TAB2.tabs(['TÙY CHỌN','CHÚ THÍCH'])
prmsContainer = st.experimental_get_query_params()

## Def lưu file
save_folder = 'Backup'
def Save_Uploaded_File (Uploadedfile):
    save_path = Path(save_folder,File.name)
    with open(save_path, mode='wb') as w:
            w.write(File.getbuffer())
    return

## Def set ZOOM

@st.cache
def getZoomFromMaxDistance(distance):
    baseZoom = 78271.484 # meter per pixel at zoom level 0 
    result = math.log(baseZoom / distance) / math.log(2)
    return np.fix(result)
@st.experimental_memo
def getGeoData(geoFileName):
    return gpd.read_file(geoFileName)

@st.cache
def getForm(name):
    return pd.read_csv(f'FormQHCT/{name}')

## Tạo điểm import file
TAB1.title("DATA IMPORT")

Files = TAB1.file_uploader("Định dạng: *.shp,*.geojson" ,accept_multiple_files=True)
if (len(prmsContainer) != 0 and list(Files) == []):
    Files = prmsContainer["file"][0]

if list(Files) == []:
    st.experimental_memo.clear()
    st.subheader("Vui lòng nhập tập tin đúng định dạng Shapefile hoặc Geojson")
else:
    if isinstance(Files, list):
        for File in Files:
            Save_Uploaded_File(File)
        Name = File.name[0:File.name.find(".")]
        End = File.name[File.name.find(".")+1:len(File.name)]
        if End == "geojson":
            X = ".geojson"
        else: X = ".shp"
        geoFileName = Name + X
    else:
        geoFileName = Files
        Name = geoFileName[0:geoFileName.find(".")]
    geoFile = f"{save_folder}/{geoFileName}"
    Df = getGeoData(geoFile)
## Chuyển tọa độ
    Df = Df.to_crs(epsg=4326)

#### Bố trí layout
    Main = st.container()

    Main.header('BẢN ĐỒ: ' + Name)
  
    # Đọc Data FormMau
    FormCoCau = getForm('FormExcelcocauSDD-QHCT.csv')
    FormChiTieu = getForm('FormExcelchitieuSDD-QHCT.csv')

    # Def group      
    def Group(DF_Group,type,F_G,F_V):
        TB = DF_Group.groupby([F_G],as_index = False)
        if type == 'first':
            TB = TB[F_V].first()
        elif type == 'sum':
            TB = TB[F_V].sum()
        elif type == 'max':
            TB = TB[F_V].max()
        elif type == 'min':
            TB = TB[F_V].min()
        elif type == 'count':
            TB = TB[F_V].value_counts()
        elif type == 'mean':
            TB = TB[F_V].mean()
        return TB

    # Def merge
    def Merge(Df_1,Df_2,L,R):
        Df_12 = pd.merge(Df_1, Df_2, how = 'left', left_on = L, right_on = R)
        return Df_12

    # Def Fiter
    def Filter(Dfa,F,List):
        df_Filter = Dfa.loc[Dfa[F].isin(List)]
        return df_Filter


    # Join thuộc tính từ FormMauCoCauSDD

    F_TK = 'MaCNCT'
    F_Color = 'MaQuyUoc'

    F_Ma = ['ChucNang','MaQuyUoc','MaNhom', 'MaDonVi', 'Color']
    chucnang = Group(FormCoCau,'first',F_TK,F_Ma)

#######################################################################################################################
    ## Thống kê tổng     

    MA = Group(Df,'first',F_TK,F_TK)

    CHUCNANG = Merge(MA,chucnang,F_TK,F_TK)

    ###Def Tính toán thống kê
    def ThongKe(df_1,Field):
    ## Tạo bảng thống kê
        SUM = ['DanSo','DienTich','DTXD','TDTSXD','SoLo','CanHo','Cho_GD']
        SUM_TK = Group(df_1,'sum',Field,SUM)
        
        ## Tính  Max - Min - Mean
        MIN = 'TCTT'
        MINTC = Group(df_1,'min',Field,MIN)
        
        MAX = ['TCTD']
        MAXTC = Group(df_1,'max',Field,MAX)

        TC = Merge(MINTC, MAXTC,Field,Field)
        ## Tính  Count
        COUNT = Group(df_1,'count',Field,Field)
        COU = Merge(TC,COUNT,Field,Field)

        CHITIEU = Merge(SUM_TK,COU,Field,Field)     
        THONGKE1 = Merge(CHUCNANG,CHITIEU,Field,Field)
        
        ## Tính chiều dài giao thông
        GT_L = Filter(df_1,'MaQuyUoc',['DGT'])
        
        GT = Group(GT_L,'max','GT_Dai',Field)
        GT_SUM = Group(GT,'sum',Field,'GT_Dai')

        ## Bảng thống kê tổng
        THONGKESDD = Merge(THONGKE1,GT_SUM,Field,Field)
        THONGKESDD.fillna(0,inplace = True)

    ## Tính bảng cơ cấu
    ## Tính diện tích
        CoCau = FormCoCau[['STT','MaCNCT', 'ChucNang']]
        TK_DT_1 = THONGKESDD[['MaCNCT', 'DienTich']]

        CoCauSDD = Merge(CoCau, TK_DT_1,Field,Field)
        TK_DT_2 = Group(THONGKESDD,'sum','MaDonVi','DienTich')

        DVO_In1 = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'DVO'].index
        DVO_In2 = TK_DT_2.loc[TK_DT_2['MaDonVi'] == 'DVO'].index
        
        NDVO_In1 = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'NDVO'].index
        NDVO_In2 = TK_DT_2.loc[TK_DT_2['MaDonVi'] == 'NDVO'].index  
        
        if list(DVO_In2) != []:
            CoCauSDD['DienTich'].values[DVO_In1] = TK_DT_2['DienTich'].values[DVO_In2]
        
        if list(NDVO_In2) != []:
            CoCauSDD['DienTich'].values[NDVO_In1] = TK_DT_2['DienTich'].values[NDVO_In2]
        
        TK_DT_3 = Group(THONGKESDD,'sum',F_Color,'DienTich')
        
        CCDVO_In1 = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'CCDVO'].index
        CCDVO_In2 = TK_DT_3.loc[TK_DT_3['MaQuyUoc'] == 'CCDVO'].index
        if list(CCDVO_In2) != []:
            CoCauSDD['DienTich'].values[CCDVO_In1] = TK_DT_3['DienTich'].values[CCDVO_In2]

        CCDT_In1 = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'CCDT'].index
        CCDT_In2 = TK_DT_3.loc[TK_DT_3['MaQuyUoc'] == 'CCDT'].index
        if list(CCDT_In2) != []:
            CoCauSDD['DienTich'].values[CCDT_In1] = TK_DT_3['DienTich'].values[CCDT_In2]

        GD1_In1 = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'GDDVO'].index
        GD1_In2 = TK_DT_3.loc[TK_DT_3['MaQuyUoc'] == 'GD1'].index
        if list(GD1_In2) != []:
            CoCauSDD['DienTich'].values[GD1_In1] = TK_DT_3['DienTich'].values[GD1_In2]
        
        HH_In1 = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'HH'].index
        HH_In2 = TK_DT_3.loc[TK_DT_3['MaQuyUoc'] == 'HH'].index
        if list(HH_In2) != []:
            CoCauSDD['DienTich'].values[HH_In1] = TK_DT_3['DienTich'].values[HH_In2]
        
        NNO_TK = Filter(TK_DT_3,F_Color,['OLK', 'OBT','OCC', 'NOXH'])
        GDDT_TK = Filter( TK_DT_3,F_Color,['GD2', 'NCDT'])

        NNO_In = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'NNO'].index
        CoCauSDD['DienTich'].values[NNO_In] = NNO_TK['DienTich'].sum()
        GDDT_In = CoCauSDD.loc[CoCauSDD['MaCNCT'] == 'GDDT'].index
        CoCauSDD['DienTich'].values[GDDT_In] = GDDT_TK['DienTich'].sum()
        CoCauSDD['DienTich'] =  round(CoCauSDD['DienTich'],2)

        ## Tính tỷ lệ
        
        DTDVO = CoCauSDD['DienTich'].values[DVO_In1].sum()
        
        DTAll = THONGKESDD['DienTich'].sum()
        
        CoCauSDD['TyLeDatO'] = nan
        CoCauSDD['TyLeToanKhu'] = nan
        
        for i in range(0, NDVO_In1.values.sum()):
            CoCauSDD.TyLeDatO[i] = round((CoCauSDD.DienTich[i]*100)/DTDVO,2)

        for m in range(0, len(CoCauSDD['MaCNCT'])):
            CoCauSDD.TyLeToanKhu[m] = round((CoCauSDD.DienTich[m]*100)/DTAll,2)

    ### Tính toán chỉ tiêu:
        ChiTieuSDD = FormChiTieu[['STT','Nhom','ChiTieu','Ma','GiaTri','DonVi']]

        # Nhóm chỉ tiêu + Dân số
        DT_Tong = THONGKESDD['DienTich'].sum()
        DS_Tong = THONGKESDD['DanSo'].sum()
        DTXD_Tong = THONGKESDD['DTXD'].sum()
        GFA_Tong = THONGKESDD['TDTSXD'].sum()
        TCTT_Tong = THONGKESDD['TCTT'].min()
        TCTD_Tong = THONGKESDD['TCTD'].max()
        # Toàn khu:
        if DS_Tong > 0:
            ChiTieuSDD['GiaTri'].values[0] = DT_Tong/DS_Tong
        
        # Đất ở
        DSDVO = THONGKESDD['DanSo'].sum()
        if DSDVO > 0:  
            ChiTieuSDD['GiaTri'].values[1] = DTDVO/DSDVO

        NNO_ChiTieu = Filter(THONGKESDD,'MaNhom',['NNO'])
        NNO_ChiTieu.reset_index(drop=True, inplace= True)
        
        DSNNO = NNO_ChiTieu['DanSo'].sum()

        DTNNO = NNO_ChiTieu['DienTich'].sum()
        
        ChiTieuSDD['GiaTri'].values[2] = DTNNO/DSNNO 

        OLK = Filter(NNO_ChiTieu,F_Color,['OLK'])
        OBT = Filter(NNO_ChiTieu,F_Color,['OBT'])
        NOXH = Filter(NNO_ChiTieu,F_Color,['NOXH'])
        OCC = Filter(NNO_ChiTieu,F_Color,['HH','OCC'])     


        DSOLK = OLK['DanSo'].sum()
        DTOLK = OLK['DienTich'].sum()

        DSOBT = OBT['DanSo'].sum()
        DTOBT = OBT['DienTich'].sum()

        DSNOXH = NOXH['DanSo'].sum()
        DTNOXH = NOXH['DienTich'].sum()

        DSOCC = OCC['DanSo'].sum()
        DTOCC = OCC['DienTich'].sum()
        

        ChiTieuSDD['GiaTri'].values[3] = DTOLK/DSOLK 
        ChiTieuSDD['GiaTri'].values[4] = DTOBT/DSOBT    
        ChiTieuSDD['GiaTri'].values[5] = DTOCC/DSOCC       
        ChiTieuSDD['GiaTri'].values[6] = DTNOXH/DSNOXH       

        # # Đất cây xanh
        CX_ChiTieu = Filter(THONGKESDD,Field,['CXDVO','CXDVO-HH'])
        
        DTCXDVO = CX_ChiTieu['DienTich'].sum()
        
        ChiTieuSDD['GiaTri'].values[7] = DTCXDVO/DSDVO

        # Đất giáo dục
        GD_ChiTieu = Filter(THONGKESDD,Field,['GD1A', 'GD1B','GD1C','GD1A-HH', 'GD1B-HH','GD1C-HH'])
        
        DTGDDVO = GD_ChiTieu['DienTich'].sum()
        
        ChiTieuSDD['GiaTri'].values[8] = DTGDDVO/DSDVO     

        # Nhóm thông tin chung

        ChiTieuSDD['GiaTri'].values[9] = DT_Tong
        ChiTieuSDD['GiaTri'].values[10] = DS_Tong
        ChiTieuSDD['GiaTri'].values[12] = DTXD_Tong*100/DT_Tong
        ChiTieuSDD['GiaTri'].values[13] = GFA_Tong/DT_Tong

        GT_ChiTieu = THONGKESDD.loc[THONGKESDD['MaNhom'] == 'DGT']
        
        GTDT = GT_ChiTieu['DienTich'].sum()
        GTCD = GT_ChiTieu['GT_Dai'].sum()
        
        ChiTieuSDD['GiaTri'].values[14] = GTDT
        ChiTieuSDD['GiaTri'].values[15] = GTCD*1000/DT_Tong

        # Nhóm thông tin tổng hợp

        CC_DTSXD = OCC['TDTSXD'].sum()
        CC_CanHo = OCC['CanHo'].sum()
        ChiTieuSDD['GiaTri'].values[16] = CC_DTSXD
        ChiTieuSDD['GiaTri'].values[17] = CC_CanHo
        
        NOXH_DTSXD = NOXH['TDTSXD'].sum()
        NOXH_CanHo = NOXH['CanHo'].sum()   
        ChiTieuSDD['GiaTri'].values[18] = NOXH_DTSXD
        ChiTieuSDD['GiaTri'].values[19] = NOXH_CanHo
        
        GD_DTSXD = GD_ChiTieu['TDTSXD'].sum()
        GD_ChoGD = GD_ChiTieu['Cho_GD'].sum()
        ChiTieuSDD['GiaTri'].values[20] = GD_DTSXD
        ChiTieuSDD['GiaTri'].values[21] = GD_ChoGD
        
        ChiTieuSDD['GiaTri'] =  round(ChiTieuSDD['GiaTri'],2)
        ChiTieuSDD.fillna(0,inplace = True)
        ChiTieuSDD['GiaTri'] = ChiTieuSDD['GiaTri'].astype(str)
        ChiTieuSDD['GiaTri'].values[11]=''.join([str(TCTT_Tong), ' - ',str(TCTD_Tong)])   

        return THONGKESDD, CoCauSDD, ChiTieuSDD


    ### Def tính toán thống kê từng khu vực theo trường thuộc tính A
    def ThongKeTheoCot(df,A):
        CoCauA = FormCoCau[['STT','MaCNCT', 'ChucNang']]
        ChiTieuA = FormChiTieu[['STT','Nhom','ChiTieu','Ma','DonVi']]
        ThongKeA = pd.DataFrame()
        List = df.sort_values(by=[A])[A]
        List.dropna(inplace = True)
        for i in pd.unique(List):
            Table = df[df[A].isin([i])]
            TKi,CCi,CTi = ThongKe(Table,'MaCNCT')
            # Cơ cấu
            CC = CCi[['MaCNCT', 'DienTich', 'TyLeDatO','TyLeToanKhu']]
            CC.columns = ['MaCNCT','DienTich - '+i,'TyLeDatO - '+i,'TyLeToanKhu - '+i]
            CoCauA = pd.merge(CoCauA, CC, how = 'left', left_on ='MaCNCT', right_on = 'MaCNCT')
            # Chỉ tiêu
            CT = CTi[['STT','GiaTri']]
            CT.columns = ['STT','GiaTri - '+i]
            ChiTieuA = pd.merge(ChiTieuA, CT, how = 'left', left_on ='STT', right_on = 'STT')
            # Thống kê
            TK = TKi
            TK = TK[TK['DienTich']>0]
            TK[A] = i
            ThongKeA = ThongKeA.append(TK,ignore_index = True)
        return CoCauA, ChiTieuA, ThongKeA

    @st.experimental_memo
    def Tinhtoan(_df,Name):
    # Tính toán các bảng thống kê, cơ cấu, chỉ tiêu:    
    # Toàn khu:
        TKToanKhu, CCToanKhu,CTToanKhu = ThongKe(_df,'MaCNCT')
        # Theo từng phân khu:
        CCPK, CTPK,TKPK = ThongKeTheoCot(_df,'PhanKhu')
        ## Theo từng đơn vị ở:
        CCDVO, CTDVO,TKDVO = ThongKeTheoCot(_df,'DVO')
        return TKToanKhu,CCToanKhu,CTToanKhu,CCPK,CTPK,TKPK,CCDVO,CTDVO,TKDVO

    ThongKeToanKhu,CoCauToanKhu,ChiTieuToanKhu,CoCauPK,ChiTieuPK,ThongKePK,CoCauDVO,ChiTieuDVO,ThongKeDVO = Tinhtoan(Df,Name)

### Đổi tên cột

    def Replace(_Table,Old,New):
        for i in range (len(Old)):
            _Table.columns = _Table.columns.str.replace(Old[i],New[i])
        return _Table
    
    def RenameColumns(CoCau,ChiTieu,ThongKe):
        CoCau = CoCau.set_index('STT')
        CoCau.fillna(0,inplace = True)
        Old_CoCau = ['MaCNCT','ChucNang','DienTich','TyLeDatO','TyLeToanKhu']
        New_CoCau = ['Mã CNCT','Chức năng','Diện tích','Tỷ lệ đơn vị ở','Tỷ lệ toàn khu']
    
        ChiTieu = ChiTieu.set_index('STT')
        Old_ChiTieu = ['Nhom','ChiTieu','Ma','DonVi','GiaTri']
        New_ChiTieu = ['Nhóm','Loại chỉ tiêu','Mã','Đơn vị','Giá trị']     

        ThongKe.index = np.arange(1, len(ThongKe)+1)
        Old_ThongKe = ['MaCNCT','ChucNang','MaQuyUoc','MaNhom','MaDonVi','Color','DanSo','DienTich','DTXD',
                    'TDTSXD','SoLo','CanHo','Cho_GD','TCTT','TCTD','count','GT_Dai','PhanKhu','DVO']
        New_ThongKe = [
            'Mã CNCT',
            'Chức năng',
            'Mã quy uớc',
            'Mã nhóm',
            'Mã đơn vị',
            'Màu hexan',
            'Dân số',
            'Diện tích',
            'Diện tích xây dựng',
            'Tổng diện tích sàn',
            'Số lô',
            'Số căn hộ',
            'Chỗ học',
            'Tầng cao tối thiểu',
            'Tầng cao tối đa',
            'Số đối tượng',
            'Chiều dài giao thông',
            'Phân khu',
            'Đơn vị ở']
        CoCau1 = Replace(CoCau,Old_CoCau,New_CoCau)
        ChiTieu1 = Replace(ChiTieu,Old_ChiTieu,New_ChiTieu)
        ThongKe1 = Replace(ThongKe,Old_ThongKe,New_ThongKe)
    
        return CoCau1,ChiTieu1,ThongKe1
    @st.experimental_memo
    def Doiten(Name):
        CoCauPK_Final,ChiTieuPK_Final,ThongKePK_Final = RenameColumns(CoCauPK,ChiTieuPK,ThongKePK)
        CoCauToanKhu_Final,ChiTieuToanKhu_Final,ThongKeToanKhu_Final = RenameColumns(CoCauToanKhu,ChiTieuToanKhu,ThongKeToanKhu)
        CoCauDVO_Final,ChiTieuDVO_Final,ThongKeDVO_Final = RenameColumns(CoCauDVO,ChiTieuDVO,ThongKeDVO)
        return CoCauPK_Final,ChiTieuPK_Final,ThongKePK_Final,CoCauToanKhu_Final,ChiTieuToanKhu_Final,ThongKeToanKhu_Final,CoCauDVO_Final,ChiTieuDVO_Final,ThongKeDVO_Final
    CoCauPK_Final,ChiTieuPK_Final,ThongKePK_Final,CoCauToanKhu_Final,ChiTieuToanKhu_Final,ThongKeToanKhu_Final,CoCauDVO_Final,ChiTieuDVO_Final,ThongKeDVO_Final = Doiten(Name)

## Tải bảng excel
    ExcelThongKe = io.BytesIO()
    with pd.ExcelWriter(ExcelThongKe, engine='xlsxwriter') as writer:
        CoCauToanKhu_Final.to_excel(writer,sheet_name = 'CoCauToanKhu')
        CoCauPK_Final.to_excel(writer, sheet_name='CoCauPK')
        CoCauDVO_Final.to_excel(writer, sheet_name='CoCauDVO')
        ChiTieuToanKhu_Final.to_excel(writer, sheet_name='ChiTieuToanKhu')       
        ChiTieuPK_Final.to_excel(writer, sheet_name='ChiTieuPK')
        ChiTieuDVO_Final.to_excel(writer, sheet_name='ChiTieuDVO')
        ThongKeToanKhu_Final.to_excel(writer, sheet_name='ThongKeToanKhu')
        ThongKePK_Final.to_excel(writer,sheet_name='ThongKePK')
        ThongKeDVO_Final.to_excel(writer,sheet_name='ThongKeDVO')        
        writer.save()
        TAB1.download_button(
            label="Tải về Excel",
            data=ExcelThongKe,
            file_name="ThongKe.xlsx",
            mime="application/vnd.ms-excel"
        )

### share button
    TAB1.write(f'Link chia sẻ: http://dataanalysis.planex.tech/?file={geoFileName}')

###########################################################################################################
## Biểu thị bản đồ
    # Lọc - xác định phạm vi Phân khu, DVO 
    F_Loc = TAB2_1.selectbox("Lọc theo", ['Toàn khu','Phân khu','Đơn vị ở'])
    if F_Loc == 'Toàn khu' : 
        Df1 = Df
        MaLoc = ""
    else: 
        if F_Loc == 'Phân khu': F_ = 'PhanKhu'
        elif F_Loc == 'Đơn vị ở': F_  = 'DVO'
        MaLoc = TAB2_1.multiselect("Thông tin lọc", pd.unique(Df[F_ ]), Df[F_ ].get(0))
        if list(MaLoc) == []:
            MaLoc = [Df[F_ ].get(0)]
        Df1 = Df[Df[F_ ].isin(MaLoc)]
        
    # Co = list(Df1)
    # Co.remove('geometry')
    # df = Df1[Co] 
    # df1 = df.select_dtypes("object")
    # AllCo = list(df)
    # Co_Str = list(df1)
    # Co_Num= [x for x in AllCo if (x not in Co_Str)]

    ## Tạo trường màu Color trong dữ liệu dựa trên MaQuyUoc 
    try:
        MAColor = Group(Df1,'first',F_TK,F_TK)
    except:
        Main.header("Vui lòng chọn dữ liệu có đúng Format")

    CHUCNANG1 = Merge(MAColor,chucnang,F_TK,F_TK)

    DF_Color = Group(CHUCNANG1,'first',F_Color,'Color')

    DF_Show = Merge(Df1,DF_Color,F_Color,F_Color)

    DF_Show.fillna(0,inplace = True)
    
    ## Chú thích mã màu
    Legend_Color1 = DF_Color
    Legend_Color2 = FormCoCau.groupby(['MaQuyUoc'],as_index = False)['ChuThich'].first()
    Legend_Color = pd.merge(Legend_Color1, Legend_Color2,how = 'left', left_on ='MaQuyUoc', right_on = 'MaQuyUoc')
    Legend_Color = Legend_Color[['Color','ChuThich']]
    Legend_Color.columns = ['Màu', 'Chức năng']
    Legend_Color.index = np.arange(1, len(Legend_Color)+1)
                
    def colordf (val):
        color = val
        return f'background-color: {color};color: {color}'
    
    Legend = Legend_Color.style.applymap(colordf,subset = ['Màu'])
    TAB2_2.dataframe(Legend,height = Legend.index.max()*38)
    
    ### Tùy chọn
    Color_Check = TAB2_1.selectbox("Cách hiển thị màu",("Màu theo Chức năng Quy hoạch","Chọn Một Màu"))

    if Color_Check == "Màu theo Chức năng Quy hoạch":
        Color = ""
        DF_Show["color"] = DF_Show['Color'].apply(hex_to_rgb)
        color_exp = 'color'

    elif Color_Check == "Chọn Một Màu":
        Color = TAB2_1.color_picker('Chọn Màu', '#FF0000')  
        DF_Show['fill'] = Color
        DF_Show["color"] = DF_Show["fill"].apply(hex_to_rgb)
        color_exp = "color"



    ## Pyeck hiển thị bản đồ   
    Map = TAB2_1.selectbox('Chọn bản đồ nền',('dark','light','road','satellite'))
    @st.experimental_memo
    def MapStyle(Type):
        MapStyle = Type
        return MapStyle
    
    
    show_3d = TAB2_1.checkbox("3D", value=False)

    if show_3d:
        ChieuCao = TAB2_1.selectbox("Chọn trường thông tin thể hiện chiều cao", ['Tầng cao tối đa','Tầng cao tối thiểu'])
        if ChieuCao == 'Tầng cao tối đa':
            Height = 'TCTD'
        else: Height = 'TCTT'
        e_scale = TAB2_1.slider("Chiều cao tầng", min_value=1, max_value=20, value=4)
        pitch = 45
    else:
        e_scale = 1
        pitch = 0
        Height = 'TCTD'
    

# Thể hiện bản đồ
    # Tạo Viewport
    Point = DF_Show.centroid
    Long = Point.x.mean()
    Lat = Point.y.mean()
    # Zoom
    tempPont = Point.to_crs(epsg=3857)
    scrWidth = 600  # fixed width, height
    scrHeight = 600 # now we using auto width, will replace after
    distance = max((tempPont.x.max(0) - tempPont.x.min()) / scrWidth, (tempPont.y.max(0) - tempPont.y.min()) / scrHeight)
    zoom = getZoomFromMaxDistance(distance)

    Viewstate = pdk.ViewState(
        latitude = Lat,
        longitude = Long,
        zoom = zoom,
        max_zoom = 20,
        pitch = pitch,
        bearing = 0,
        # width = scrWidth,
        height = scrHeight    
    )

    ## Tạo label
    @st.experimental_memo
    def tooltip(x):
        tool={"html": """
            <p style="font-size:12px;margin-bottom:4px">Mã Ô phố: {MaOPho}</p>
            <p style="font-size:12px;margin-bottom:4px">Chức Năng: {ChucNang}</p>
            <p style="font-size:12px;margin-bottom:4px">Dân số: {DanSo}</p>
            <p style="font-size:12px;margin-bottom:4px">Diện tích (m2): {DienTich}</p>
            <p style="font-size:12px;margin-bottom:4px">Diện tích xây dựng (m2): {DTXD}</p>
            <p style="font-size:12px;margin-bottom:4px">Tổng diện tích sàn (m2): {TDTSXD}</p>
            <p style="font-size:12px;margin-bottom:4px">Tầng cao tối thiểu: {TCTT}</p>
            <p style="font-size:12px;margin-bottom:4px">Tầng cao tối đa: {TCTD}</p>
            <p style="font-size:12px;margin-bottom:4px">Mật độ xây dựng (%): {MDXD}</p>
            <p style="font-size:12px;margin-bottom:4px">Hệ số sử dụng đất: {HSSDD}</p>
            <p style="font-size:12px;margin-bottom:4px">Số lô đất: {SoLo}</p>
            <p style="font-size:12px;margin-bottom:4px">Số căn hộ: {CanHo}</p>
            <p style="font-size:12px;margin-bottom:4px">Số chỗ học: {Cho_GD}</p>
        """}
        return tool

    ## Add Layer
    @st.experimental_memo
    def DataGeo(F1,F2,e_scale,Height,show_3d,Color_check,Color):
        Geo = pdk.Layer(
            "GeoJsonLayer",
            data=DF_Show,
            get_position='-',
            radius=200,
            elevation_scale=e_scale,
            get_elevation= Height,
            pickable=True,       
            extruded=show_3d,
            filled = True,
            wireframe=True,
            get_fill_color=color_exp,
            get_line_color=["0,0,0"],)
        return Geo
    ## Hiển Thị

    m = pdk.Deck(
        layers = [DataGeo(F_Loc,MaLoc,e_scale,Height,show_3d,Color_Check,Color)],
        initial_view_state = Viewstate, 
        map_provider='mapbox', 
        map_style= MapStyle(Map), 
        tooltip = tooltip(""))
    
    HienBanDo = TAB2.checkbox('TẮT/HIỆN BẢN ĐỒ',True)
    if HienBanDo == True:
        Main.pydeck_chart(m,use_container_width=True)

### Bố trí
    # Main2.header("BẢNG - BIỂU")
    col1,col2 = Main.columns((3.5,6.5))
    Col1 =col1.container()
    Col2 =col2.container()


 #### Bảng dữ liệu
## Biểu thị bản đồ
    # Lọc - xác định phạm vi Phân khu, DVO 
  
    Loc_Table = TAB3.selectbox("Thống kê theo", ['Toàn khu','Chọn khu vực','So sánh'])

    if Loc_Table == 'Toàn khu':
        ThongKe_Table = ThongKeToanKhu_Final
        ChiTieu_Table = ChiTieuToanKhu_Final
        CoCau_Table = CoCauToanKhu_Final
    elif Loc_Table == 'Chọn khu vực':
        F_Loc2 = TAB3.selectbox("Chọn theo", ['Phân khu','Đơn vị ở'])
        if F_Loc2 == 'Phân khu': F = 'PhanKhu'
        elif F_Loc2 == 'Đơn vị ở': F = 'DVO'
        MaLoc2 = TAB3.multiselect("Thông tin chọn", pd.unique(Df[F]), Df[F].get(0))
        if list(MaLoc2) == []:
            MaLoc2 = [Df[F].get(0)]
        Df2 = Df[Df[F].isin(MaLoc2)]
        TK_Table, CC_Table, CT_Table = ThongKe(Df2,'MaCNCT')
        CoCau_Table,ChiTieu_Table,ThongKe_Table = RenameColumns(CC_Table,CT_Table,TK_Table)
        
    elif Loc_Table == 'So sánh':

        ## Bảng cơ cấu
        CC0 = FormCoCau[['MaCNCT','TenNhom']]
        CC0 = CC0.loc[CC0['TenNhom'].isnull() == False]
        CC0.columns = ['Mã CNCT','Tên nhóm chức năng']
        CC1 = CoCauToanKhu_Final[['Mã CNCT','Diện tích']]
        CC1.columns = ['Mã CNCT', 'Diện tích - Tổng']
        A_ = CoCauPK_Final.filter( like = 'Tỷ lệ')
        List1 = [x for x in list(CoCauPK_Final) if (x not in list(A_))]
        CC2 = CoCauPK_Final[List1]
        B_ = CoCauDVO_Final.filter( like = 'Tỷ lệ')
        List2 = [x for x in list(CoCauDVO_Final) if (x not in list(B_))]
        CC3 = CoCauDVO_Final[List2]
        CC4 = Merge(CC0,CC1,'Mã CNCT','Mã CNCT')
        CC5 = Merge(CC4,CC2,'Mã CNCT','Mã CNCT')
        CC6 = Merge(CC5,CC3,'Mã CNCT','Mã CNCT')
        List3 = list(CC6.filter(like = 'Diện tích'))
        CC7 = CC6.groupby(['Tên nhóm chức năng'],as_index = False)[List3].sum()

        ## Bảng chỉ tiêu
        CT0 = ChiTieuToanKhu_Final[['Nhóm','Loại chỉ tiêu','Đơn vị','Giá trị']]
        CT0.columns = ['Nhóm','Loại chỉ tiêu','Đơn vị','Giá trị - Tổng']
        CT1 = ChiTieuPK_Final.drop(['Nhóm','Mã','Đơn vị'], axis=1)
        CT2 = ChiTieuDVO_Final.drop(['Nhóm','Mã','Đơn vị'], axis=1)
        CT3 = Merge(CT0,CT1,'Loại chỉ tiêu','Loại chỉ tiêu')
        CT4 = Merge(CT3,CT2,'Loại chỉ tiêu','Loại chỉ tiêu')

        ## So sánh:
        L_Tong = ['Tổng']
        L_PK = pd.unique(Df['PhanKhu'])
        L_DVO = [x for x in pd.unique(Df['DVO']) if str(x) == x]
        L_ALL = [*L_Tong, *L_PK, *L_DVO]
        
        
        ### Chọn cột so sánh
        
        F_Loc3 = TAB3.multiselect("So sánh giữa",L_ALL,L_ALL[0:2])
        
        L_1 = ['Tên nhóm chức năng']
        L_2 = ['Nhóm','Loại chỉ tiêu','Đơn vị']
        L_3 = []
        for i in F_Loc3:
            x = 'Diện tích - '+ i
            y = 'Giá trị - '+ i
            L_1.append(x)
            L_2.append(y)
            CC7['Diện tích - '+i] = round(CC7['Diện tích - '+i],2)
            CC7['Tỷ lệ - '+i] = round(CC7['Diện tích - '+i]*100/CC7['Diện tích - '+i].sum(),2)
            L_3.append('Tỷ lệ - '+i)

        CoCau_HienThi = CC7[[*L_1,*L_3]]
        CoCau_HienThi.index = np.arange(1, len(CoCau_HienThi)+1)
        ChiTieu_HienThi = CT4[L_2]
        ChiTieu_HienThi.index = np.arange(1, len(ChiTieu_HienThi)+1)
 
    if Loc_Table == 'So sánh':
        CoCau_Final = CoCau_HienThi
        ChiTieu_Final = ChiTieu_HienThi
        ChuGiai_Tron = FormCoCau[['TenNhom','Color_Nhom']]
        ChuGiai_Tron = Group(ChuGiai_Tron,'first','TenNhom',['Color_Nhom'])
        Table_Tron = Merge(CoCau_Final,ChuGiai_Tron,'Tên nhóm chức năng','TenNhom')
    else:
        ## Bảng thống kê
        ThongKe_Final = ThongKe_Table[[x for x in list(ThongKe_Table) if (x not in ['Mã CNCT','Mã quy ước','Mã nhóm','Mã đơn vị','Màu hexan'])]]

        ## Bảng chỉ tiêu
        ChiTieu_HienThi = ChiTieu_Table[['Nhóm','Loại chỉ tiêu','Đơn vị','Giá trị']]
        ## Tính toán bảng cơ cấu
        TyLeDienTich = ThongKe_Table.groupby(['Mã nhóm'],as_index = False)['Diện tích'].sum()
        ChuGiai_Tron = FormCoCau[['MaNhom','TenNhom','Color_Nhom']]
        ChuGiai_Tron = ChuGiai_Tron.groupby(['MaNhom'],as_index = False)[['TenNhom','Color_Nhom']].first()
        Table_Tron = pd.merge(TyLeDienTich, ChuGiai_Tron, how = 'left', left_on ='Mã nhóm', right_on = 'MaNhom')
        CoCau_HienThi = Table_Tron[['TenNhom','Diện tích']]
        CoCau_HienThi['TyLe'] = CoCau_HienThi['Diện tích']*100/CoCau_HienThi['Diện tích'].sum()
        CoCau_HienThi['TyLe'] = round(CoCau_HienThi['TyLe'],2)
        CoCau_HienThi.columns = ['Tên nhóm chức năng','Diện tích','Tỷ lệ']
        CoCau_HienThi.index = np.arange(1, len(CoCau_HienThi)+1) 

    ## Bảng thống tin chung
    BangThongTinChung = ChiTieu_HienThi.loc[ChiTieu_HienThi['Nhóm'] == 'Nhóm thông tin chung']
    BangThongTinChung_HienThi =  BangThongTinChung[[x for x in list(ChiTieu_HienThi) if x != 'Nhóm']]
    BangThongTinChung_HienThi.index = np.arange(1,len(BangThongTinChung_HienThi)+1)

    ## Bảng thông tin tổng hợp
    BangThongTinTongHop = ChiTieu_HienThi.loc[ChiTieu_HienThi['Nhóm'] == 'Nhóm thông tin tổng hợp']
    BangThongTinTongHop_HienThi =  BangThongTinTongHop[[x for x in list(ChiTieu_HienThi) if x != 'Nhóm']]
    BangThongTinTongHop_HienThi.index = np.arange(1,len(BangThongTinTongHop_HienThi)+1)
    
    ## Bảng chỉ tiêu
    BangChiTieu = ChiTieu_HienThi.loc[ChiTieu_HienThi['Nhóm'] == 'Nhóm chỉ tiêu']
    BangChiTieu_HenThi =  BangChiTieu[[x for x in list(ChiTieu_HienThi) if x != 'Nhóm']]
    

    ## Hiển thị biểu đồ: 
    ## Def biểu đồ  
    def BD_Tron(label,value,colors,Name):
        fig_Tron = go.Figure(data=[go.Pie(labels=label, values=value,text = value,hole=.3)])
        fig_Tron.update_traces(
            textposition = "inside",
            hoverinfo='label+value+percent',
            textinfo='percent', 
            textfont_size=12,
            marker=dict(
                colors=colors, 
                line=dict(color='#000000', width=1)
            )
        )
        fig_Tron.update_layout(
            template = None,
            uniformtext_minsize=12,
            uniformtext_mode='hide',
            title=dict(
                text=Name,
                # x=0.5,
                # y=0.95,
                font=dict(
                    # family="Arial",
                    size=35,
                    # color='#0000AA'
                )
            )
        )
        return fig_Tron

## Tùy chọn biểu đồ
    if Loc_Table != 'So sánh':
        BieuDo = TAB3.selectbox("Chọn nhóm thông tin", ['Cơ cấu','Chỉ tiêu','Thống kê'])
        if BieuDo == 'Cơ cấu':
            ## Bảng
            Col1.header('Bảng cơ cấu')
            Col1.header("")
            Col1.dataframe(CoCau_HienThi,height = 320)
            
            ## Biểu 
            Label_Tron = list(Table_Tron['TenNhom'])
            Value_Tron = list(Table_Tron['Diện tích'])
            Mau = Table_Tron['Color_Nhom']
            Name = 'Cơ Cấu SDD'
            Col2.plotly_chart(BD_Tron(Label_Tron,Value_Tron,Mau,Name),use_container_width=True)
              
        elif BieuDo == 'Chỉ tiêu':
            ## Bảng
            
            Col1.header('Bảng chỉ tiêu')
            Col1.header("")
            Col1.dataframe(BangChiTieu_HenThi,height = 320)
                
            ## Biểu
            
            BDCot = BangChiTieu_HenThi
            BDCot['ChuGiai'] = ['Toàn khu','Đơn vị ở', 'Nhóm ở', 'Liền kề', 'Biệt thự', 'Chung cư', 'Nhà ở xã hội', 'Cây xanh', 'Giáo dục']
            BDC_List = list(BDCot.filter(like = 'Giá trị'))
            LocChiTieu = TAB3.multiselect("Chọn chỉ tiêu", pd.unique(BDCot['ChuGiai']),BDCot['ChuGiai'])
            BDCot = BDCot.loc[BDCot['ChuGiai'].isin(LocChiTieu)]
            labels = BDCot['ChuGiai']
            
            colors = TAB3.color_picker('Chọn Màu', '#00FFFF')
            fig_Cot = go.Figure(layout={'yaxis': {'title': 'Chỉ Tiêu (m2/người)'}})
            
            for i in BDC_List:
                value = BDCot[i].astype(float)
                BDCot.fillna(0,inplace = True)
                fig_Cot.add_trace(go.Bar(
                    x=labels,
                    y=list(value),
                    name=i,
                    text = value,
                    marker_color=colors
                ))
            fig_Cot.update_traces(
                marker_line_color='#000000',
                textfont_size=12,
                marker_line_width=1, 
                opacity=0.8,
                texttemplate='%{text:.2s}', 
                textposition='outside',
                hovertemplate="<br>".join(["%{x}:%{text}"])
            )
            fig_Cot.update_layout(template = None ,barmode='group',uniformtext_minsize=12, uniformtext_mode='hide',height=500,
                title=dict(
                    text='<b>Chỉ tiêu</b>',
                    font=dict(
                        size=35,
                    )
                )
            )
            Col2.plotly_chart(fig_Cot,use_container_width=True)
            
        elif BieuDo == 'Thống kê':
            ## Bảng thông tin chung
            
            
            Col1.header('Bảng thông tin chung')
            Col1.dataframe(BangThongTinChung_HienThi,height = 280)
            ## Bảng thông tin tổng hợp
            Col1.header('Bảng thông tin tổng hợp')
            Col1.dataframe(BangThongTinTongHop_HienThi,height = 250) 
            ## Bảng thống kê chi tiết
            Col2.header('Bảng thống kê')
            Col2.dataframe(ThongKe_Final, height = 620)
    else:

        BieuDo = TAB3.selectbox("Chọn nhóm thông tin", ['Cơ cấu','Chỉ tiêu','Chung','Tổng hợp'])
        if BieuDo == 'Cơ cấu':
            BD_Type = TAB3.selectbox('Loại biểu đồ',['Tròn','Cột Chồng', 'Cột'])
            if BD_Type == 'Tròn':
            ## Bảng
                Col1.header('Bảng cơ cấu')
                Col1.header("")
                Col1.dataframe(CoCau_HienThi,height = 320)
                LSS = list(Table_Tron.filter(like = 'Diện tích'))
                BDCoCau = Table_Tron[['Tên nhóm chức năng',*LSS]] 
                ## Biểu  tròn
                
                Label_Tron = list(Table_Tron['Tên nhóm chức năng'])
                colors = Table_Tron['Color_Nhom']
                
                ## Tính toán số cột, hàng
                C = math.ceil(len(F_Loc3)/2)
                if C <2: C = C+1
                cols = TAB3.slider('Số bản đồ trong một hàng',1,5,C)
                rows = math.ceil(len(F_Loc3)/cols)
                if rows == 0: rows = 1
                
                subplot_titles = [F_Loc3[i] for i in range(len(F_Loc3))]
                
                specs = [[{'type':'domain'}]*cols]*rows
                
                fig_Tron = make_subplots(rows = rows,cols = cols, specs = specs,subplot_titles=subplot_titles)
                
                for i,l in enumerate(LSS):
                    row = i//cols + 1
                    col = i%cols+1
            
                    fig_Tron.add_trace(go.Pie(
                        labels = Label_Tron,
                        values = BDCoCau[l],
                        text = BDCoCau[l]
                    ),
                    row = row,col = col)
                    
                fig_Tron.update_traces(
                    textposition = "inside",
                    hoverinfo='label+value+percent',
                    textinfo='percent', 
                    textfont_size=12,
                    marker=dict(
                        colors=colors, 
                        line=dict(color='#000000', width=1)
                    )
                )
                fig_Tron.update_layout(
                    template = None,height=600,
                    uniformtext_minsize=12,
                    uniformtext_mode='hide',
                    title=dict(
                        text='<b>Cơ cấu SDD</b>',
                        font=dict(
                            size=35,
                        )
                    )
                )
                Col2.plotly_chart(fig_Tron,use_container_width=True)

                                      
            if BD_Type == "Cột Chồng":
                # Bảng
                Col1.header('Bảng cơ cấu')
                Col1.header("")
                Col1.dataframe(CoCau_HienThi)
                
                
                LSS = list(Table_Tron.filter(like = 'Diện tích'))
                
                colors = Table_Tron[['Tên nhóm chức năng','Color_Nhom']]
                colors = colors.set_index('Tên nhóm chức năng')
                colors = colors.T
                
                BDCoCau = Table_Tron[['Tên nhóm chức năng',*LSS]]
                BDCotChong = BDCoCau.set_index('Tên nhóm chức năng')
                BDCotChong = BDCotChong.T
                Labels = BDCotChong.columns[BDCotChong.dtypes == float]
                
                fig = go.Figure()
                for i in Labels:
                    y1 = BDCotChong[i]
                    fig.add_traces(go.Bar(x=F_Loc3, y=y1, name=i, text = y1,
                                          marker=dict(color=list(colors[i])[0],line=dict(color='#000', width=1))
                                          )
                                   )
    
                fig.update_traces(
                            texttemplate='%{value:.2f}%',
                            textfont_size=12,
                            opacity=0.8,
                            textposition='inside',
                            insidetextanchor='middle',
                            hovertemplate=("<br>Diện tích: %{text:.2f} m2"+
                                            "<br>Tỷ lệ: %{value:.2f}%"),
                            )
                fig.update_layout(template = None,height=500,
                            barmode='stack', barnorm = 'percent',
                            uniformtext_minsize=12,
                            uniformtext_mode='hide',
                            title=dict(
                                text='<b>Cơ cấu SDD</b>',
                                font=dict(
                                    family='Arial',
                                    size=35,
                                    )
                                ),
                            yaxis=dict(
                                ticksuffix="%",
                                title = 'Tỷ lệ (%)'
                                )
                        )

                Col2.plotly_chart(fig,use_container_width=True)
                
                              
            if BD_Type == "Cột":
                # Bảng
                Col1.header('Bảng cơ cấu')
                Col1.header("")
                Col1.dataframe(CoCau_HienThi,height = 320)
                # Biểu
                LSS = list(Table_Tron.filter(like = 'Diện tích'))
                BDCoCau = Table_Tron[['Tên nhóm chức năng',*LSS]] 
                x = list(Table_Tron['Tên nhóm chức năng'])
                fig = go.Figure(layout={'yaxis': {'title': 'Diện tích (m2)'}})
                for i in LSS:
                    value = BDCoCau[i].astype(float)
                    fig.add_traces(go.Bar(x=x, y=value, name=i[11:len(i)],text = value))
                    
                fig.update_traces(
                                marker_line_color='#000000',
                                textfont_size=12,
                                marker_line_width=1.5, 
                                opacity=0.8,
                                texttemplate='%{text:.2f}', 
                                textposition='outside',
                                hovertemplate="<br>".join(["%{x} :%{text} m2"])
                                )
                fig.update_layout(template = None ,barmode='group',uniformtext_minsize=12, uniformtext_mode='hide',height=500,
                title=dict(
                    text='<b>Diện tích</b>',
                    font=dict(
                        size=30,
                    )
                )
            )
                Col2.plotly_chart(fig, use_container_width=True)
            
        # x = TAB3.multiselect("Chọn tiêu chí", list(Table_Tron.filter(lik
        # e = 'Diện tích')))
        # y = list(Table_Tron['Tên nhóm chức năng'])
        # data = Table_Tron[['TenNhom','Diện tích - Tổng','Diện tích - A']]
        # data = data.set_index('TenNhom')
        # data = data.T
        # data = data.reset_index()
        # fig = px.bar(data,x = list(Table_Tron.filter(like = 'Diện tích')),y = list(Table_Tron['TenNhom']))
        # fig.update_layout(title = "CCSDD", xaxis_title = 'Customer Condition', yaxis_title =  'Percentage')
        # st.plotly_chart(fig,use_container_width=True)
        # st.dataframe(data)

        elif BieuDo == 'Chỉ tiêu':
            ## Bảng
            
            Col1.header('Bảng chỉ tiêu')
            Col1.header("")
            Col1.dataframe(BangChiTieu_HenThi)
            
            ## Biểu
            BDCot = BangChiTieu_HenThi
            BDCot['ChuGiai'] = ['Toàn khu','Đơn vị ở', 'Nhóm ở', 'Liền kề', 'Biệt thự', 'Chung cư', 'Nhà ở xã hội', 'Cây xanh', 'Giáo dục']
            BDC_List = list(BDCot.filter(like = 'Giá trị'))
    
            LocChiTieu = TAB3.multiselect("Chọn chỉ tiêu", pd.unique(BDCot['ChuGiai']),['Nhóm ở','Nhà ở xã hội', 'Cây xanh','Giáo dục'])
            BDCot = BDCot.loc[BDCot['ChuGiai'].isin(LocChiTieu)]
            labels = BDCot['ChuGiai'] 
            BD_Type = TAB3.selectbox('Loại biểu đồ',['Cột','Đường'])
            if BD_Type == 'Cột':
                fig_Cot = go.Figure(layout={'yaxis': {'title': 'Chỉ Tiêu (m2/người)'}})
                for i in BDC_List:
                    Name = i[10:len(i)]
                    value = BDCot[i].astype(float)
                    BDCot.fillna(0,inplace = True)
                    fig_Cot.add_trace(go.Bar(
                        x=labels,
                        y=list(value),
                        name=Name,
                        text = value
                    ))
                fig_Cot.update_traces(
                    marker_line_color='#000000',
                    textfont_size=12,
                    marker_line_width=1.5, 
                    opacity=0.8,
                    texttemplate='%{text:.2s}', 
                    textposition='outside',
                    hovertemplate="<br>".join(["%{x}:%{text}"])
                )
                fig_Cot.update_layout(template = None ,barmode='group',uniformtext_minsize=12, uniformtext_mode='hide',height=500,
                    title=dict(
                        text='<b>Chỉ tiêu</b>',
                        font=dict(
                            size=35,
                        )
                    )
                )
                Col2.plotly_chart(fig_Cot,use_container_width=True)
            elif BD_Type == "Đường":
                BDDuong = BDCot[['ChuGiai',*BDC_List]]
                BDDuong = BDDuong.set_index("ChuGiai")
                BDDuong = BDDuong.T
                BDDuong = BDDuong.reset_index()
                fig_Duong = go.Figure(layout={'yaxis': {'title': 'Chỉ Tiêu (m2/người)'}})
                for i in [x for x in list(BDDuong) if x != 'index']:
                    Name = i
                    value = BDDuong[i].astype(float)
                    BDDuong.fillna(0,inplace = True)
                    fig_Duong.add_trace(go.Scatter(
                        x=list(BDDuong['index']),
                        y=list(value),
                        name=Name,
                        # text = value
                    ))
                
                fig_Duong.update_layout(template = None,uniformtext_minsize=12, uniformtext_mode='hide',height=500,
                    title=dict(
                        text='<b>Chỉ tiêu</b>',
                        font=dict(
                            size=35,
                        )
                    )
                )
                Col2.plotly_chart(fig_Duong,use_container_width=True)

        elif BieuDo == 'Chung':
            ## Bảng
            Col1.header('Bảng thông tin chung')
            Col1.header('')
            Col1.dataframe(BangThongTinChung_HienThi)
            
            ## Biểu
            
            BDChung = BangThongTinChung_HienThi
            BDChung['ChuGiai'] = BDChung['Loại chỉ tiêu']
            BDC_List = list(BDChung.filter(like = 'Giá trị'))
            Loc_Chung = TAB3.selectbox("Chọn chỉ tiêu", [x for x in list(BDChung['ChuGiai']) if x != 'Tầng cao xây dựng toàn khu'])
            BDChung = BDChung.loc[BDChung['ChuGiai'].isin([Loc_Chung])]
            labels = BDC_List
            
            ## Đảo ma trận hàng - cột
            BDChung = BDChung.set_index('ChuGiai')
            BDChung = BDChung.filter(like = "Giá trị").T
            value = list(BDChung[Loc_Chung])
            
            ## Tùy chọn màu
            colors = TAB3.color_picker('Chọn Màu', '#FF6600')
            
            fig_Cot = go.Figure(layout={'yaxis': {'title': Loc_Chung }})
            fig_Cot.add_trace(go.Bar(
                    x=labels,
                    y=value,
                    name=i,
                    text = value,
                    marker_color=colors
                ))
            fig_Cot.update_traces(
                marker_line_color='#000000',
                textfont_size=12,
                marker_line_width=1.5, 
                opacity=0.8,
                texttemplate='%{text:.2s}', 
                textposition='outside',
                hovertemplate="<br>".join(["%{x} :%{text}"]))
            fig_Cot.update_layout(template = None ,barmode='group',uniformtext_minsize=12, uniformtext_mode='hide',height=500,
                title=dict(
                    text='<b>Thông tin chung</b>',
                    font=dict(
                        size=35,
                    )
                )
            )
            Col2.plotly_chart(fig_Cot,use_container_width=True)
        
        elif BieuDo == 'Tổng hợp':
            ## Bảng
            Col1.header('Bảng thông tin tổng hợp')
            Col1.header("")
            Col1.dataframe(BangThongTinTongHop_HienThi)  
            
            ## Biểu
            BDTongHop = BangThongTinTongHop_HienThi
            BDTongHop['ChuGiai'] = BDTongHop['Loại chỉ tiêu']
            BDC_List = list(BDTongHop.filter(like = 'Giá trị'))
            Loc_TH = TAB3.selectbox("Chọn chỉ tiêu", BDTongHop['ChuGiai'])
            BDTongHop = BDTongHop.loc[BDTongHop['ChuGiai'].isin([Loc_TH])]
            labels = BDC_List
            
            ## Đảo ma trận hàng - cột
            BDTongHop = BDTongHop.set_index('ChuGiai')
            BDTongHop = BDTongHop.filter(like = "Giá trị").T
            
            value = list(BDTongHop[Loc_TH])
            
            ## Tùy chọn màu
            colors = TAB3.color_picker('Chọn Màu', '#FF6600')
            
            fig_Cot = go.Figure(layout={'yaxis': {'title': Loc_TH }})
            fig_Cot.add_trace(go.Bar(
                    x=labels,
                    y=value,
                    name=i,
                    text = value,
                    marker_color=colors
                ))
            fig_Cot.update_traces(
                marker_line_color='#000000',
                
                textfont_size=12,
                marker_line_width=1.5, 
                opacity=0.8,
                texttemplate='%{text:.2s}', 
                textposition='outside',
                hovertemplate="<br>".join(["%{x}: %{text}"])
            )
            fig_Cot.update_layout(template = None ,barmode='group',uniformtext_minsize=12, uniformtext_mode='hide',height=500,
                title=dict(
                    text='<b>Thông tin tổng hợp</b>',
                    font=dict(
                        size=35,
                    )
                )
            )
            Col2.plotly_chart(fig_Cot,use_container_width=True)
   
    
            