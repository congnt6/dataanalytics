#from asyncio.windows_events import NULL
from itertools import chain
from tkinter import FALSE
import streamlit as st
import pandas as pd
import numpy as np
import folium
#from streamlit_folium import folium_static
import geopandas as gpd
import pathlib
import fiona
import json
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import io
import xlsxwriter
from pathlib import Path
import glob
from distutils.filelist import FileList
#from msilib.schema import File




## Def lưu file
save_folder ='data'
def Save_Uploaded_File (Uploadedfile):
    save_path = Path(save_folder,File.name)
    with open(save_path, mode='wb') as w:
            w.write(File.getbuffer())
    return

## Tạo điểm import file
st.sidebar.title("DATA ANALYSYS")

Files = st.sidebar.file_uploader("Lựa chọn tệp tin" ,accept_multiple_files=True)

if list(Files) ==[]:
    st.subheader("Vui lòng nhập tập tin đúng định dạng Shapefile hoặc Geojson")
    st.write("Khuyến cáo nếu có các trường thông tin thì theo format sau:")
    st.write("DienTich: Diện tích (Float)")
    st.write("DanSo: Dân số (Float)")
    st.write("DTXD: Diện tích xây dựng (Float)")
    st.write("TDTSXD: Tổng diện tích xây dựng (Float)")
    st.write("MDXD: Mật độ xây dựng (Float)")
    st.write("HSSDD: Hệ số sử dụng đất (Float)")
    st.write("TCTT: Tầng cao tối thiểu (Int/Float)")
    st.write("TCTD: Tầng cao tối đa (Int/Float)")   
else: 
    for File in Files:
        Save_Uploaded_File(File)
    Name = File.name[0:File.name.find(".")]
    End = File.name[File.name.find(".")+1:len(File.name)]
    if End == "geojson":
        X = ".geojson"
    else: X = ".shp"

    Df = gpd.read_file(save_folder+"/"+Name + X)

## Chuyển tọa độ
    Df = Df.to_crs(epsg=4326)

## Lọc trường thông tin dữ liệu
    Co = list(Df)
    Co.remove('geometry')
    df = Df[Co]
    st.sidebar.subheader('Lọc Thông Tin')
    
##!!!!!!!!!!!!!!!!! Bổ sung lọc theo khoảng > =, < = đối với trường thuộc tính có loại int, float
    all = st.sidebar.checkbox("Chọn tất cả", value=False)
    if all:
        df1 = df 
    else:
# ## Lọc bảng thuộc tính theo trường
        F_Loc = st.sidebar.selectbox("Trường thông tin cần lọc", pd.unique(list(df))) 
        MaLoc = st.sidebar.multiselect("Thông tin lọc", pd.unique(df[F_Loc]) )
        df1 = df[df[F_Loc].isin(MaLoc)]
    Doit = st.checkbox("Bảng thông tin lọc:", value=FALSE)
    if Doit:
        st.dataframe(df1)
    else: st.write("")
 
    # st.download_button(label = "Tải về", data=df1.to_csv(),file_name="Loc.csv", mime = 'text/csv')

# ## Hiển thị dữ liệu lên map
#     ## Tạo bản đồ
#     BanDo = st.checkbox("Hiện bản đồ", value=False)
#     if BanDo == False:
#         st.write("")
#     else:
#         m = folium.Map(location=[10.7, 106.6], tiles='CartoDB positron', name="Light Map",
#                 zoom_start=15, attr="My Data attribution")
#         folium.Choropleth(
#             geo_data=Df,
#             name="choropleth",
#             fill_color='#ffffff',
#             line_color='#000000',
#             fill_opacity=0.1,
#             line_opacity=0.7,
#         ).add_to(m)
#         ## Tạo popup
#         folium.features.GeoJson(Df,
#                             name="QH", popup=folium.features.GeoJsonPopup(fields=Co)).add_to(m)
#         folium_static(m, width=600, height=400)

# ##### Thống kê theo trường mã tùy chọn
    Thongke = st.sidebar.checkbox("Thống kê", value=False)
    if Thongke:
        F_TK = st.sidebar.selectbox("Chọn trường mã cần thống kê", pd.unique(list(df1)))
        
    ## Thống kê tổng   
        SUM = st.sidebar.multiselect("Chọn trường tính toán tổng", pd.unique(list(df1)) )
        SUM_TK = df1.groupby([F_TK],as_index = False)[SUM].sum()

    # MAXMDXD.rename({'MDXD': 'MDXD_Max'}, axis =1, inplace = True)
    ## Tính  Max - Min - Mean
        MIN = st.sidebar.multiselect("Chọn trường tính toán MIN", pd.unique(list(df1)) )
        MINTC = df1.groupby([F_TK], as_index = False)[MIN].min()
        
        MAX = st.sidebar.multiselect("Chọn trường tính toán MAX", pd.unique(list(df1)) )
        MAXTC = df1.groupby([F_TK], as_index = False)[MAX].max()
        
        MEAN = st.sidebar.multiselect("Chọn trường tính toán MEAN", pd.unique(list(df1)) )
        MEANTC = df1.groupby([F_TK], as_index = False)[MEAN].mean()

        TC = pd.merge(MINTC, MAXTC,how = 'left', left_on =F_TK, right_on = F_TK)
        TC1 = pd.merge(TC, MEANTC,how = 'left', left_on =F_TK, right_on = F_TK)
        
    ## Tính  Count
        COUNT = st.sidebar.checkbox("Đếm giá trị (COUNT)", value=False)
        if COUNT:
            COUNT = df1.groupby([F_TK], as_index = False)[F_TK].value_counts()
            COU = pd.merge(TC1, COUNT,how = 'left', left_on =F_TK, right_on = F_TK)
            THONGKESDD = pd.merge(SUM_TK, COU, how = 'left', left_on =F_TK, right_on = F_TK)
        else: THONGKESDD = pd.merge(SUM_TK, TC1, how = 'left', left_on =F_TK, right_on = F_TK)
        
    ## Bảng thống kê sử dụng đất

        Doit = st.checkbox("Bảng thống kê:", value=FALSE)
        if Doit:
            st.dataframe(THONGKESDD)
        else: st.write("")
        # st.download_button(label = "Tải về", data=THONGKESDD.to_csv(),file_name="ThongKe.csv", mime = 'text/csv')
    else: st.write("")


    ExcelThongKe = io.BytesIO()
    with pd.ExcelWriter(ExcelThongKe, engine='xlsxwriter') as writer:
        df1.to_excel(writer, sheet_name='Loc')
        if Thongke:
            THONGKESDD.to_excel(writer, sheet_name='ThongKe')
        else:""
        writer.save()
        st.download_button(
        label="Tải về Excel Lọc - Thống Kê",
        data=ExcelThongKe,
        file_name="ThongKe.xlsx",
        mime="application/vnd.ms-excel")
        
        

###     Hiển thị biểu đồ
    BieuDo = st.sidebar.checkbox("Biểu Đồ", value=False)
    if BieuDo:
        Type = st.sidebar.radio("Chọn loại biểu đồ", ['Biểu đồ tròn', 'Biểu đồ cột'])
    ## Biểu đồ Tròn
        if Type == "Biểu đồ tròn":
            F_BD = st.sidebar.selectbox("Chọn trường cần thể hiện biểu đồ", pd.unique(list(THONGKESDD)))
            labels = list(THONGKESDD[F_TK])
            values = list(THONGKESDD[F_BD])

            fig = go.Figure(data=[go.Pie(labels=labels, values=values,hole=.3)])
            fig.update_traces(
                textposition = "inside",
                hoverinfo='label+value+percent',
                textinfo='percent', 
                textfont_size=12,
                marker=dict(
                #   colors=colors, 
                line=dict(color='#000000', width=1)))
            fig.update_layout(uniformtext_minsize=12, uniformtext_mode='hide')
            st.plotly_chart(fig)
        
        
    ## Biểu đồ Cột
        elif Type == "Biểu đồ cột":
            SoCot= st.sidebar.radio("Chọn số cột:", ['Một', 'Hai'])
        ## Biểu đồ một cột
            if SoCot == "Một":
                F_BD = st.sidebar.selectbox("Chọn trường cần thể hiện biểu đồ", pd.unique(list(THONGKESDD)))
                labels = list(THONGKESDD[F_TK])
                values = list(THONGKESDD[F_BD])
                fig = go.Figure(
                    data=[
                        go.Bar(name = F_BD, x=labels, y=values, yaxis='y', offsetgroup=1,marker_color='#FF6600', text = values),
                    ],
                    layout={'yaxis': {'title': F_BD}}
                )
                fig.update_traces(marker_line_color='#000000',
                                marker_line_width=1.5, opacity=0.8,texttemplate='%{text:.2s}', textposition='outside',
                                hovertemplate="<br>".join([
                                    "%{x}:%{text}"]))

                fig.update_layout(barmode='group',uniformtext_minsize=12, uniformtext_mode='hide')
                st.plotly_chart(fig)
                
                
        ## Biểu đồ hai cột
            elif SoCot == "Hai":
                F_BD1 = st.sidebar.selectbox("Chọn trường 1 cần thể hiện biểu đồ", pd.unique(list(THONGKESDD)))
                F_BD2 = st.sidebar.selectbox("Chọn trường 2 cần thể hiện biểu đồ", pd.unique(list(THONGKESDD)))
                labels = list(THONGKESDD[F_TK])
                value1 = list(THONGKESDD[F_BD1])
                value2 = list(THONGKESDD[F_BD2])

                fig = go.Figure(
                    data=[
                        go.Bar(name=F_BD1, x=labels, y=value1, yaxis='y1', offsetgroup=1,marker_color='#FF6600', text = value1,),
                        go.Bar(name=F_BD2, x=labels, y=value2, yaxis='y2', offsetgroup=2,marker_color='#009900', text = value2,)
                    ],
                    layout={
                        'yaxis': {'title': F_BD1},
                        
                        'yaxis2': {'title': F_BD2, 'overlaying': 'y', 'side': 'right'}
                    }
                )
                fig.update_traces(marker_line_color='#000000',
                                marker_line_width=1.5, opacity=0.8,texttemplate='%{text:.2s}', textposition='outside',
                                hovertemplate="<br>".join([
                                    "%{x}:%{text}"]))
                fig.update_layout(barmode='group',uniformtext_minsize=12, uniformtext_mode='hide')
                st.plotly_chart(fig)
            else: st.write("")
        else: st.write("")

            
            
    else: st.write("")


         



