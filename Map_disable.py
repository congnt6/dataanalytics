from asyncio.windows_events import NULL
from doctest import DocFileTest
from itertools import chain
from tkinter import FALSE
from tkinter.tix import AUTO
from turtle import width
import streamlit as st
import pandas as pd
import numpy as np
import folium
from streamlit_folium import folium_static
import geopandas as gpd
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import io
import xlsxwriter
from pathlib import Path
from distutils.filelist import FileList
from msilib.schema import File
import pydeck as pdk
import leafmap.colormaps as cm
from leafmap.common import hex_to_rgb


## Def lưu file
save_folder = 'C:\\Users\\VLAB\Desktop\\Streamlit\\Backup'
def Save_Uploaded_File (Uploadedfile):
    save_path = Path(save_folder,File.name)
    with open(save_path, mode='wb') as w:
            w.write(File.getbuffer())
    return


## Def chuyển màu Hexan sang RGB
# def hex_to_rgb(h):
#     h = h.lstrip("#")
#     return tuple(int(h[i : i + 2], 16) for i in (0, 2, 4))

## Tạo điểm import file
st.sidebar.title("DATA VISUALIZATION")

Files = st.sidebar.file_uploader("Lựa chọn tệp tin" ,accept_multiple_files=True)

if list(Files) ==[]:
    st.subheader("Vui lòng nhập tập tin đúng định dạng Shapefile hoặc Geojson")
    st.write("Khuyến cáo nên có các trường thông tin theo format sau:")
    st.write("DienTich: Diện tích (Float)")
    st.write("DanSo: Dân số (Float)")
    st.write("DTXD: Diện tích xây dựng (Float)")
    st.write("TDTSXD: Tổng diện tích xây dựng (Float)")
    st.write("MDXD: Mật độ xây dựng (Float)")
    st.write("HSSDD: Hệ số sử dụng đất (Float)")
    st.write("TCTT: Tầng cao tối thiểu (Int/Float)")
    st.write("TCTD: Tầng cao tối đa (Int/Float)")   
else: 
    for File in Files:
        Save_Uploaded_File(File)
    Name = File.name[0:File.name.find(".")]
    End = File.name[File.name.find(".")+1:len(File.name)]
    if End == "geojson":
        X = ".geojson"
    else: X = ".shp"

    Df = gpd.read_file(save_folder+"/"+Name + X)

## Chuyển tọa độ, lọc trường geometry
    Df = Df.to_crs("epsg:4326")
    
    Co = list(Df)
    Co.remove('geometry')
    df = Df[Co]
       
    df1 = df.select_dtypes("object")
    AllCo = list(df)
    Co_Str = list(df1)
    Co_Num= [x for x in AllCo if (x not in Co_Str)]

## Dựng 3D bằng Pydeck

    Height = st.sidebar.selectbox("Chọn trường thông tin thể hiện chiều cao", Co_Num)
    Color_Check = st.sidebar.selectbox("Cách hiển thị màu",("Chọn Một Màu", "Màu hiển thị theo chiều cao", "Màu theo trường Tùy chọn","Chọn trường có mã màu Hexan"))
    if Color_Check == "Chọn trường có mã màu Hexan":
        Color = st.sidebar.selectbox("Chọn trường thông tin thể hiện màu Hexan", Co_Str)
        try:
            Df["color"] = Df[Color].apply(hex_to_rgb)
            color_exp = 'color'
        except:
            st.header("Vui lòng chọn đúng trường Color")

    elif Color_Check == "Chọn Một Màu":
        Color = st.sidebar.color_picker('Pick A Color', '#FF0000')  
        Df['fill'] = Color
        Df["color"] = Df["fill"].apply(hex_to_rgb)
        color_exp = "color"

    elif Color_Check == "Màu hiển thị theo chiều cao":
        # Tạo Color palettes
        palettes = cm.list_colormaps()
        palette = st.sidebar.selectbox("Color palette", palettes, index=palettes.index("Blues"))
        n_colors = st.sidebar.slider("Number of colors", min_value=1, max_value=20, value= 8)
        colors = cm.get_palette(palette, n_colors)
        colors = [hex_to_rgb(c) for c in colors]

        ## Sort để giá trị height theo chiều tăng dần
        gdf = Df.sort_values(by=Height, ascending=True)

        ## Loại bỏ các trùng lặp, đưa ra list các giá trị height tăng dần   
        selectdf = gdf.groupby([Height], as_index = False)[Height].first()       

        ## fill màu của các nhóm màu tương ứng qua các giá trị height       
        for i, ind in enumerate(selectdf.index):    
            index = int(i / (len(selectdf) / len(colors)))
            if index >= len(colors):
                index = len(colors) - 1
            selectdf.loc[ind, "R"] = colors[index][0]
            selectdf.loc[ind, "G"] = colors[index][1]
            selectdf.loc[ind, "B"] = colors[index][2]    
        ## Vlookup màu giá trị height vào Dataframe
        Df = pd.merge(gdf, selectdf,how = 'left', left_on =Height, right_on = Height)
        color_exp = f'[R, G, B]'

    elif Color_Check == "Màu theo trường Tùy chọn":
        Co_Color = st.sidebar.selectbox("Chọn trường thông tin hiển thị màu", AllCo)
        
        ## Sort để giá trị height theo chiều tăng dần
        gdf = Df.sort_values(by=Co_Color, ascending=True)

        ## Loại bỏ các trùng lặp, đưa ra list các giá trị height tăng dần   
        selectdf = gdf.groupby([Co_Color], as_index = False)[Co_Color].first()   

        # Tạo Color palettes
        palettes = cm.list_colormaps()
        palette = st.sidebar.selectbox("Color palette", palettes, index=palettes.index("Blues"))
        n_colors = selectdf[Co_Color].count()
        colors = cm.get_palette(palette, n_colors)
        colors = [hex_to_rgb(c) for c in colors]

        ## fill màu của các nhóm màu tương ứng qua các giá trị height       
        for i, ind in enumerate(selectdf.index):    
            index = int(i / (len(selectdf) / len(colors)))
            if index >= len(colors):
                index = len(colors) - 1
            selectdf.loc[ind, "R"] = colors[index][0]
            selectdf.loc[ind, "G"] = colors[index][1]
            selectdf.loc[ind, "B"] = colors[index][2]    
        ## Vlookup màu giá trị height vào Dataframe
        Df = pd.merge(gdf, selectdf,how = 'left', left_on =Co_Color, right_on = Co_Color)
        color_exp = f'[R, G, B]'       

    ## Pyeck hiển thị bản đồ   
    Map = st.sidebar.selectbox('Chọn bản đồ nền',('None','light','dark','road','satellite'))
    if Map == 'None':
        Mapprovider = 'None'
    else:
        Mapprovider = 'mapbox'

    Label = st.sidebar.selectbox("Chọn trường hiển thị thông tin", AllCo)

    show_3d = st.sidebar.checkbox("Show 3D view", value=False)
    if show_3d:
        e_scale = st.sidebar.slider("Elevation scale", min_value=1, max_value=20, value= 1)
        pitch = 45
    else:
        e_scale = 1
        pitch = 0
    ## Tạo Viewport
    Point = Df.centroid
    Long = Point.x.mean()
    Lat = Point.y.mean()
    Zoom = (Point.x.max() - Point.x.min())*10/(Point.y.max() - Point.y.min())

    View = pdk.View(type= 'FirstPersonView', controller=True, width=600, height=600)
    Viewstate = pdk.ViewState(latitude=Lat, longitude=Long, zoom = Zoom, max_zoom=20, pitch=pitch, bearing=0)
   
   
   
    ## Tạo label
    Df['Label'] = Df[Label]
    Df['ChieuCao'] = Df[Height]

    tooltip={"text": "Label: {Label}; Height: {ChieuCao}"}

    ## Add Layer
    Geo = pdk.Layer(
            "GeoJsonLayer",
            data=Df,
            get_position='-',
            radius=200,
            elevation_scale=e_scale,
            get_elevation=Height,
            pickable=True,       
            extruded=show_3d,
            filled = True,
            wireframe=True,
            get_fill_color=color_exp,
            get_line_color=["0,0,0"],
        )


    ## Hiển Thị   
    m = pdk.Deck(layers = [Geo], views= View, initial_view_state = Viewstate, map_provider=Mapprovider, map_style= Map , tooltip = tooltip)       
    
    st.pydeck_chart(m)

    

    
    
